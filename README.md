#How to guide
This guide is compiled in the assumtion that the test are run on an OS X machine.
Also the browser used is Google Chrome
##Pre-requisites 
* Homebrew: `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`
* Chromedriver: `brew cask install chromedriver`
* Allure: `brew install allure`

##Running tests
* Run tests: `mvn clean test`
* To see report: `allure serve target/surefire-reports/`