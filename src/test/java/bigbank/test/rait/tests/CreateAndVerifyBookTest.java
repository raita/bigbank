package bigbank.test.rait.tests;

import bigbank.test.rait.helpers.Helpers;
import bigbank.test.rait.helpers.models.Book;
import bigbank.test.rait.pageobjects.allbooks.AllBooksPage;
import bigbank.test.rait.pageobjects.booksummary.BookSummaryPage;
import bigbank.test.rait.pageobjects.createbook.CreateBookPage;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.yandex.qatools.allure.annotations.Description;

import java.util.concurrent.TimeUnit;

public class CreateAndVerifyBookTest {
    WebDriver driver;
    String createBookURL = "https://raamatukogu.herokuapp.com/catalog/book/create";
    String listBooksURL = "https://raamatukogu.herokuapp.com/catalog/books";

    CreateBookPage createBookPage;
    BookSummaryPage bookSummaryPage;
    AllBooksPage allBooksPage;

    Helpers helpers = new Helpers();
    Book newBook;

    @Before
    public void initTest(){
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        newBook = Book.builder()
                .title(helpers.generateRandomTitle())
                .summary("lorem ipsum dolores")
                .isbn(helpers.timestamp())
                .genre("Fiction").build();
    }

    @After
    public void tearDown(){
        driver.close();
    }

    @Test
    @Description("Create new book test")
    public void createBookTest(){
        driver.get(createBookURL);

        createBookPage = new CreateBookPage(driver);
        bookSummaryPage = new BookSummaryPage(driver);
        allBooksPage = new AllBooksPage(driver);

        newBook.setAuthor(createBookPage.getFirstAuthor());

        createBookPage.fillBookDetailsAndSubmit(newBook);
        bookSummaryPage.assertTitle("Title: "+newBook.getTitle()).assertAuthor(newBook.getAuthor());

        driver.get(listBooksURL);
        allBooksPage.clickLinkByText(newBook.getTitle())
                .assertTitle("Title: "+newBook.getTitle())
                .assertAuthor(newBook.getAuthor());

    }

}
