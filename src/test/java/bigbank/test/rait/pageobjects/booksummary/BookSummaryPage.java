package bigbank.test.rait.pageobjects.booksummary;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BookSummaryPage {
    WebDriver driver;

    By h1 = By.tagName("h1");
    By contentArea = By.className("col-sm-10");



    public BookSummaryPage(WebDriver driver){
        this.driver = driver;
    }

    public BookSummaryPage assertAuthor(String value){
        String authorLink = driver.findElement(contentArea).findElement(By.tagName("a")).getText();
        Assert.assertEquals(value, authorLink);

        return this;
    }

    public BookSummaryPage assertTitle(String value){
        String title = driver.findElement(h1).getText();
        Assert.assertEquals(value, title);

        System.out.println(title);

        return this;
    }
}
