package bigbank.test.rait.pageobjects.allbooks;

import bigbank.test.rait.pageobjects.booksummary.BookSummaryPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class AllBooksPage {
    WebDriver driver;
    By contentBlock = By.className("col-sm-10");
    By link = By.tagName("a");

    public AllBooksPage(WebDriver driver){
        this.driver = driver;
    }

    public List<WebElement> getAllLinks(){
        return driver.findElement(contentBlock).findElements(link);
    }

    public BookSummaryPage clickLinkByText(String linkText){
        getAllLinks().forEach(link -> {
            if (link.getText().equals(linkText)){
                link.click();
            }
        });
        return new BookSummaryPage(driver);
    }
}
