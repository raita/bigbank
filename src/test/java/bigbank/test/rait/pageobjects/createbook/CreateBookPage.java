package bigbank.test.rait.pageobjects.createbook;

import bigbank.test.rait.helpers.models.Book;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

public class CreateBookPage {
    WebDriver driver;

    By titleBox = By.id("title");
    By authorList = By.id("author");
    By summaryBox = By.id("summary");
    By ISBNBox = By.id("isbn");
    By genreCheckBox = By.name("genre");
    By submitButton = By.xpath("//button[@type='submit']");

    public CreateBookPage(WebDriver driver){
        this.driver = driver;
    }

    public List<WebElement> getAllAuthors(){
        WebElement authors = driver.findElement(authorList);
        Select select = new Select(authors);

        return select.getOptions();
    }

    public String getFirstAuthor(){
        return getAllAuthors().get(0).getText();
    }

    @Step
    public CreateBookPage enterTitle(String title){
        driver.findElement(titleBox).sendKeys(title);
        return this;
    }

    public CreateBookPage selectRandomAuthor(){
        //TODO: select random author
        return this;
    }

    @Step("Type summary")
    public CreateBookPage enterSummary(String summery){
        driver.findElement(summaryBox).sendKeys(summery);
        return this;
    }

    @Step
    public CreateBookPage enterISBN(String isbn){
        driver.findElement(ISBNBox).sendKeys(isbn);
        return this;
    }

    public CreateBookPage selectGenre(String genre){
        //TODO: how to select correct genre
        return this;
    }

    @Step("Click submit")
    public CreateBookPage clickSubmit(){
        driver.findElement(submitButton).click();
        return this;
    }

    public CreateBookPage fillBookDetailsAndSubmit(Book book){
        this.enterTitle(book.getTitle())
                .enterSummary(book.getSummary())
                .enterISBN(book.getIsbn())
                .selectGenre(book.getGenre())
                .clickSubmit();
        return this;
    }

}
