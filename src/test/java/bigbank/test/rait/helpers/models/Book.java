package bigbank.test.rait.helpers.models;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
public class Book {

    @Getter @Setter
    private String title;

    @Getter @Setter
    private String summary;

    @Getter @Setter
    private String isbn;

    @Getter @Setter
    private String author;

    @Getter @Setter
    private String genre;
}
